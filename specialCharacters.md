### Special Characters

* **\h** the hostname up to the first .
* **\n** newline
* **\s** the name of the shell
* **\t** the current time in the 24-hour format
* **\u** the username of the current working directory
* **\w** the current working directory
* **\W** the basename of the current working directory 
