# PROMPT VARIABLE COLOR
green=$(tput setaf 113);
purple=$(tput setaf 177);
blue=$(tput setaf 117);
yellow=$(tput setaf 228);
reset=$(tput sgr0);

#GIT BRANCH IN PROMPT
#github.com/joseluisq/terminal-git-branch-name.md
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

#PROMPT export message
PS1="\[${green}\]\u"; #user
PS1+="\[${purple}\]@Barrel$ "; #host
PS1+="\[${blue}\]in";
PS1+="\n"
PS1+="\[${reset}\]\W"; #working directory
PS1+="\[${yellow}\] \$(parse_git_branch)"; #gitbranch
PS1+="\[${reset}\] $ ";

export PS1;
